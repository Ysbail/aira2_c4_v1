import numpy as np


def wind_speed(u, v):
    return np.sqrt(np.power(u, 2) + np.power(v, 2))
    
    
def wind_dir(u, v):
    return (3 * np.pi / 2 - np.arctan2(v, u)) % (2 * np.pi) * 180 / np.pi