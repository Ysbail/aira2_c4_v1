import numpy as np
import netCDF4 as nc
import glob

import src.ncHelper as ncHelper
import src.varHelper as varHelper

def get_dataset(date_path, frc_range=None, _norms={}):
    
    ### TODO parse levels and types, not hard-coded
    levels = np.append(np.arange(1000., 725., -25.), np.arange(700., 150., -50.))
    
    if frc_range is None:
        frc_slice = slice(0, 24)
    else:
        frc_slice = slice(frc_range[0], frc_range[1])
    
    types = {'3d': 
                   {'slice': [frc_slice, slice(0, levels.size)],
                    'vars': ['hgtprs', 'tmpprs', 'rhprs', 'dptprs'],
                    'winds': ['ugrdprs', 'vgrdprs'], # u, v
                    'dims': ('time', 'level', 'latitude', 'longitude'),
                   },
             'sfc':
                   {'slice': [frc_slice, ],
                    'vars': ['tmpsfc', 'tmp2m', 'capesfc', 'rh2m',
                             'wind_direction', 'wind_magnitude',
                             'acpcpsfc', 'apcpsfc', 'pratesfc',
                             'cpratsfc', 'crainsfc', 'pwatclm',
                             'tcolrclm',
                            ],
                    'winds': [], # u, v
                    'dims': ('time', 'latitude', 'longitude'),
                   }
            }
    
    input_dataset = None
    
    # date_path = dtNow.strftime(PATH)
    
    if len(glob.glob(date_path.format(_type='*'))) != 2:
        raise 'do not contain both 3d and sfc data for the given path %s'%date_path
    
    feat_idx = 0
    for _type, rules in types.items():
        type_path = date_path.format(_type=_type)
        nc_file = nc.Dataset(type_path, 'r', keepweakref=True)
        
        if input_dataset is None:
            num_features = 145 # TODO automate based on given types
            times = ncHelper.load_time(nc_file['time'])[frc_slice, ]
            lat, lon = ncHelper.get_lats_lons_file(nc_file, skip=1, trim=0)
            input_dataset = np.zeros((times.size, lat.size, lon.size, num_features))
            
        for var in rules['vars']:
            print(var)
            _temp = nc_file.variables[var][rules['slice']] / _norms.get(var, 1.)
            if len(_temp.shape) == 4:
                _slice = slice(feat_idx, feat_idx + _temp.shape[1])
                _temp = _temp.transpose((0, 2, 3, 1))
                input_dataset[..., _slice] = _temp
                feat_idx += _temp.shape[-1]
            else:
                input_dataset[..., feat_idx] = _temp
                feat_idx += 1
            
        if len(rules.get('winds', [])) == 2:
            u = nc_file.variables[rules['winds'][0]][rules['slice']]
            v = nc_file.variables[rules['winds'][1]][rules['slice']]
            
            # wind_speed
            _temp = varHelper.wind_speed(u, v) / _norms.get('wind_speed', 1.)
            if len(_temp.shape) == 4:
                _slice = slice(feat_idx, feat_idx + _temp.shape[1])
                _temp = _temp.transpose((0, 2, 3, 1))
                input_dataset[..., _slice] = _temp
                feat_idx += _temp.shape[-1]
            else:
                input_dataset[..., feat_idx] = _temp
                feat_idx += 1
            
            # wind_dir
            _temp = varHelper.wind_dir(u, v) / _norms.get('wind_dir', 1.)
            if len(_temp.shape) == 4:
                _slice = slice(feat_idx, feat_idx + _temp.shape[1])
                _temp = _temp.transpose((0, 2, 3, 1))
                input_dataset[..., _slice] = _temp
                feat_idx += _temp.shape[-1]
            else:
                input_dataset[..., feat_idx] = _temp
                feat_idx += 1
                
        nc_file.close()
                
    return input_dataset, times, lat, lon
    
class IterDataset:
    def __init__(self, path, frc_range=None, _norms={}, step=24):
        self.path = path
        self.frc_range = frc_range
        self._norms = _norms
        
        self.idx = 0
        self.step = step
        self.tuples = tuple([(i, min(i + step, frc_range)) for i in range(0, frc_range, step)])
        self.n_tuples = len(self.tuples)
        
    def __iter__(self, ):
        self.idx = 0
        return self
        
    def __next__(self, ):
        if self.idx == self.n_tuples:
            raise StopIteration
        res = get_dataset(self.path, self.tuples[self.idx], self._norms)
        self.idx += 1
        return res
