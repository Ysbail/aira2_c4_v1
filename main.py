# prepare wrf dataset in-memory?
# prepare model
# run forecast
# save forecast.dot

# from memory_profiler import profile

import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

import numpy as np
import netCDF4 as nc
import datetime as dt
from keras.models import load_model

import src.load_wrf as load_wrf
import src.ncBuilder as ncBuilder

PATH = '/storagefapesp/data/modelos/WRF/27.0/%Y%m/M0/00/%Y%m%d00_0478S0865W0147N0194W_{_type}.nc4'
OUT_PATH = '/storagefinep/aira2/c4/%Y%m%d_aira2_c4.nc'
MODEL_PATH = './models/aira2_c4_v1.h5'

_norms = {'hgtprs': 5800., 
          'tmpprs': 204., 
          'rhprs': 100., 
          'dptprs': 298., 
          'wind_speed': 30., 
          'wind_dir': 360., 
          'tmpsfc': 305., 
          'tmp2m': 310., 
          'rh2m': 100., 
          'wind_direction': 360., 
          'wind_magnitude': 30., 
          'acpcpsfc': 150., 
          'cpratsfc': 150.,
          'apcpsfc': 100.,
          'crainsfc': 100.,
          'pratesfc': 100., 
          'capesfc': 50.,
          }

# @profile
def gen_nc(dtNow):
    date_path = dtNow.strftime(PATH)
    frc_range = 120
    # input_dataset, times, lat, lon = load_wrf.get_dataset(date_path, frc_range, _norms)
    input_dataset = load_wrf.IterDataset(date_path, frc_range, _norms, step=24)
    out_nc_file = nc.Dataset(dtNow.strftime(OUT_PATH), 'w')
    
    model = load_model(MODEL_PATH)
    
    times = []
    yhat = []
    ydot = []
    for _input, time, lat, lon in input_dataset:
        _temp = model.predict(_input)
        yhat.append(_temp)
        ydot.append(_temp.dot([0, 15, 30, 100]))
        times.append(time)
        
    ydot = np.concatenate(ydot, axis=0)
    yhat = np.concatenate(yhat, axis=0)
    times = np.concatenate(times, axis=0)
    
    print('check shapes:', times.shape, yhat.shape, ydot.shape)
    
    _vars = {'dot': {'dims': ('time', 'latitude', 'longitude'),
                     'dtype': np.float64,
                     'long_name': 'expected rain',
                     'standard_name': 'expected rain',
                     'units': 'mm',
                    },
             'c0': {'dims': ('time', 'latitude', 'longitude'),
                    'dtype': np.float64,
                    'long_name': 'expected rain between 0mm and 1mm',
                    'standard_name': 'expected rain between 0mm and 1mm',
                    'units': 'P(X=0)',
                   },
             'c1': {'dims': ('time', 'latitude', 'longitude'),
                    'dtype': np.float64,
                    'long_name': 'expected rain between 1mm and 15mm',
                    'standard_name': 'expected rain between 1mm and 15mm',
                    'units': 'P(X=1)',
                   },
             'c2': {'dims': ('time', 'latitude', 'longitude'),
                    'dtype': np.float64,
                    'long_name': 'expected rain between 15mm and 30mm',
                    'standard_name': 'expected rain between 15mm and 30mm',
                    'units': 'P(X=2)',
                   },
             'c3': {'dims': ('time', 'latitude', 'longitude'),
                    'dtype': np.float64,
                    'long_name': 'expected rain between 30mm and 100mm',
                    'standard_name': 'expected rain between 30mm and 100mm',
                    'units': 'P(X=3)',
                   },
            }
    
    ncBuilder.createNC(out_nc_file, dtNow, lat, lon, time=times, vars=_vars)
    ncBuilder.update_NC(out_nc_file, 'dot', ydot, )
    ncBuilder.update_NC(out_nc_file, 'c0', yhat[..., 0], )
    ncBuilder.update_NC(out_nc_file, 'c1', yhat[..., 1], )
    ncBuilder.update_NC(out_nc_file, 'c2', yhat[..., 2], )
    ncBuilder.update_NC(out_nc_file, 'c3', yhat[..., 3], )
    
    out_nc_file.close()


if __name__ == '__main__':
    dtNow = dt.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0) # - dt.timedelta(days=1)
    gen_nc(dtNow)
    # for dtNow in [dtNow + dt.timedelta(days=-i) for i in range(14)][::-1]: gen_nc(dtNow)
